<!DOCTYPE hmtl>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 3</title>
</head>
<body>
    <h2>Días de la semana en un array</h2>
<?php

echo "<hr> Vamos a mostrar los días de la semana que hemos añadido a un array, en el que los introducimos todos de golpe <br>";

$array1 = array ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");

//la podemos recorrer con el bucle foreach de dos maneras:
echo "<br> Recorrido mediante bucle foreach simple <br><br>";
foreach ($array1 as $element){
    echo $element . '<br>';
}
//foreach clave->elemento
echo "<hr> Recorrido mediante bucle foreach con posicion->elemento <br><br>";
foreach ($array1 as $position=>$element){
    echo $position . ": " . $element . '<br>';
}

$array2[]= "Lunes";
$array2[]= "Martes";
$array2[]= "Miércoles";
$array2[]= "Jueves";
$array2[]= "Viernes";
$array2[]= "Sábado";
$array2[]= "Domingo";

echo "<br>";

//la podemos recorrer con el bucle foreach de dos maneras:
echo "<hr>Ahora los hemos introducido de uno en uno <br>";
echo "<br>Recorrido mediante bucle foreach simple <br><br>";

foreach ($array2 as $element){
    echo $element . '<br>';
}
//foreach clave->elemento
echo "<hr> Recorrido mediante bucle foreach con posicion->elemento <br><br>";
foreach ($array2 as $position=>$element){
    echo $position . ": " . $element . '<br>';
}

echo "<hr>Ahora los metemos en una tabla<br>";

$array3 = array ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");

echo "<br><table border=1px>";
foreach ($array3 as $element){
echo "<tr>";
echo "<td>";
echo $element. '<br>';
echo "</td>";
echo "</tr>";
}
echo "</table>";


echo "<hr>Ahora los metemos en una lista<br>";

$array4 = array ("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");

echo "<br><ul>";
foreach ($array4 as $element){
echo "<li>";
echo $element. '<br>';
echo "</li>";
}
echo "</ul>";

?>
</body>
</html>
