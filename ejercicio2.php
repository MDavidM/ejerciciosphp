<!DOCTYPE hmtl>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 2</title>
</head>
<body>
    <h2>Precio productos</h2>
<?php
$precioUnidad=250;
$cantidad=3;
$iva=0.21;
$precioSinIva=$cantidad*$precioUnidad;
$precioConIva=$iva*($precioUnidad*$cantidad)+$precioSinIva;

echo "El precio del producto a comprar es de: $precioUnidad € <br>";
echo "Vamos a comprar: " .$cantidad. " de este producto. <br>";
echo 'El precio sin Iva total será de: ' .$precioSinIva. "<br>";
echo 'El precio con Iva total será de: ' .$precioConIva. '<br>';

?>
</body>
</html>
