<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Deseos</title>
</head>
<body>
    <h1>Lista de deseos(sesiones) de: <?php echo isset($_SESSION['user']) ? $_SESSION['user'] : '' ?></h1>
    <h3>Pide tu deseo</h3>
    <p><a href="?method=close">Cerrar sesión</a></p>
    <form method="post" action="?method=new">
        <label>Deseo:</label>
        <input type="text" name="deseo">
        <input type="submit" value="enviar">
        <hr>
        <h3>Lista de deseos</h3>
        <ul>
            <?php foreach ($deseos as $clave => $deseo): ?>
                <li> <?php echo $deseo ?> <a href="?method=delete&key=<?php echo $clave ?>">Borrar deseo</a> </li>

            <?php endforeach ?>
            <a href="?method=deleteAll">Borrar todos los deseos</a>
        </ul>
    </form>
</body>
</html>
