<!DOCTYPE hmtl>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 4</title>
</head>
<body>
    <h2>Array asociativo equipo baloncesto</h2>
<?php

$array1 = array (
    'alero' => 'Joan Sastre',
    'escolta' => 'Juan Carlos Navarro',
    'base' => 'Ricky Rubio',
    'pivot' => 'Pau Gasol',
    'ala-pivot' => 'Pierre Oriola'
);

//foreach clave->elemento
echo "Recorrido mediante bucle foreach con posicion->elemento <br><br>";
foreach ($array1 as $position=>$element){
    echo $position . ": " . $element . '<br>';
}

?>
</body>
</html>
