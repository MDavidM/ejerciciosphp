<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login para calculadora</title>
</head>
<body>
    <h1>Login</h1>
    <h3>Conectate</h3>
    <form method="post" action="?method=auth">
        <label>Usuario:</label>
        <input type="text" name="user" value="<?php echo isset($user) ? $user : '' ?>">
        <input type="submit" value="Entrar">
        <!-- Si dejamos el usuario en blanco y le damos a entrar, se quedará en esta misma página, has de poner un usuario -->
    </form>
</body>
</html>
