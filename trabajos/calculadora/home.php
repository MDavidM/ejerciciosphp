<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <title>Calculadora</title>
</head>
<body>
    <h1>Calculadora de <?php echo isset($_SESSION['user']) ? $_SESSION['user'] : '' ?></h1>
    <p>Realiza tu operación, asegurate de poner ambos operandos y seleccionar el operador que necesites.</p>
    <form method="post" action="?method=operacion">
        <label>Operando 1:</label>
        <input type="text" name="operando1">
        <label>Selecciona el operador:</label>
        <select name="operador" >
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
        </select>
        <label>Operando 2:</label>
        <input type="text" name="operando2">
        <input type="submit" value="Calcular">
    </form>
    <br>
    <hr>
    <p><a href="?method=close">Cerrar sesión</a></p>
    <hr>
    <?php if (isset($_SESSION['mensajes'])): ?>
        <?php foreach ($_SESSION['mensajes'] as $mensaje): ?>
            <?php echo $mensaje ?>
        <?php endforeach ?>
    <?php endif ?>
    <!-- Si nos tiene que devolver algún mensaje de error lo hará en esta misma página y nos dejará en ella para saber que falló y seguir realizando operaciones -->
</body>
</html>
