<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Resultado</title>
</head>
<body>
    <h1>Resultado</h1>
    <?php echo "El resultado a tu operación es: $resultado" ?>
    <hr>
    <p><a href="index.php?method=home">Volver atrás</a></p>
    <!-- con esto podemos volver atrás y seguir haciendo operaciones -->
    <p><a href="?method=close">Cerrar sesión</a></p>
    <!-- con esto cerramos sesión si no queremos hacer más operaciones -->
</body>
</html>
