<?php

class App
{
    function __construct()
    {
        session_start();
    }// function __construct
    // iniciar la sesión.

    public function login()
    {
        require('login.php');
    }//funcion login login
    // iniciar una sesión.

    public function auth()
    {
        // tomar usuario y meter en sesión.
        if (isset($_REQUEST['user']) && !empty($_REQUEST['user']))
        {
            $user = $_REQUEST['user'];
            $_SESSION['user'] = $user;
            header('Location:index.php?method=home');// si el usuario inicia bien, continuamos hacia la pagina home.
        }else{
            header('Location:index.php?method=login');// sino, volvemos al login, en este caso, si le damos a "Entrar" sin poner nombre de usuario.
        }// if - else
        return;
    }// funcion auth

    public function home()
    {
        if (!isset($_SESSION['user'])){
            header('Location:index.php?method=login');
            return;
        }// if
        $user = $_SESSION['user'];
        require('home.php');// nos lleva al home si iniciamos la sesión.
        unset($_SESSION['mensajes']);// quitamos los mensajes para que no se muestren siempre.
    }// funcion home

    public function close()
    {
        session_destroy();
        unset($_SESSION);
        header('Location:index.php?method=login');
    }// para cerrar la sesión desde la página de resultados si no queremos hacer más operaciones.

    public function operacion()
    {
        if (!isset($_REQUEST['operando1'])){
            $_SESSION['mensajes'][] = "<br>Operando 1 no introducido.";
        }else{
            $operando1 = $_REQUEST['operando1'];
        }// if - else

        if (!isset($_REQUEST['operando2'])){
            $_SESSION['mensajes'][] = "<br>Operando 2 no introducido.";
        }else{
            $operando2 = $_REQUEST['operando2'];
        }// if - else
        // aquí varios problemas ocurrieron, esta fue la mejor solución, usar el !isset y no usar el empty, ya que al usar el empty si dabamos un número 0, lo detectaba como NO introducido. ahora el problema que ocurre es que estos mensajes de error no los muestra nunca (los de arriba) porque siempre detecta algo introducido, aún que no sea el caso (que dejemos la caja en blanco) pero si hacemos eso (dejar la caja en blanco, nos mostrará el error de abajo, por lo que tampoco es tan grave...

        if (!is_numeric($operando1)){
            $_SESSION['mensajes'][] = "<br>El operando 1 debe ser número.";
        }// if
        // si no es un número lo que introduzcamos saltará este error.

        if (!is_numeric($operando2)){
            $_SESSION['mensajes'][] = "<br>El operando 2 debe ser número.";
        }// if
        // igual que el anterior.

        if (isset($_SESSION['mensajes'])) {
            header('Location:?method=home');
            return;
        }// if
        // si hay mensajes vuelta al formulario con header.

        $resultado=0;
        $operando1 = $_REQUEST['operando1'];
        $operando2 = $_REQUEST['operando2'];
        $operador = $_REQUEST['operador'];

        if($operador == "+"){
            $resultado = $operando1 + $operando2;
        }else if($operador == "-"){
            $resultado = $operando1 - $operando2;
        }else if($operador == "*"){
            $resultado = $operando1 * $operando2;
        }else if($operador == "/"){
            if ($operando2 == "0" && $operador == "/"){
             $_SESSION['mensajes'][]= "<br>Solo Chuck Norris puede dividir entre 0";
             if (isset($_SESSION['mensajes'])) {
                header('Location:?method=home');
                return;// si intentamos dividir entre 0, saltará este error, y nos devolvera al formulario.
            }// if
        }else{
            $resultado = $operando1 / $operando2;
        } // else
    }// else if
    require('resultado.php');// si todo funciona bien te lleva a la página resultado.
}// funcion operacion

}// class App
