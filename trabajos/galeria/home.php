<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Galería</title>
</head>
<body>
  <h1>Galería de fotos</h1>
  <form enctype="multipart/form-data" method="post" action="?method=subir">
   <input name="ficheroSubido" type="file" />
   <input type="submit" value="Subir archivo"/>
 </form>
 <br>
 <?php foreach ($arrayFotos as $imagen): ?>
  <img src=<?php echo "uploads/$imagen" ?> width="100">
  <ul>
    <?php echo $imagen ?>
  </ul>
  <form method="post" action="?method=detalle">
    <input type="hidden" name="detalle" value="<?php echo "uploads/$imagen" ?>">
    <input type="submit" name="Ampliar" value="Ampliar">
  </form>
  <br>
  <form method="post" action="?method=borrar">
    <input type="hidden" name="borrar" value="<?php echo "uploads/$imagen" ?>">
    <input type="submit" name="Eliminar" value="Eliminar">
  </form>
  <hr>
<?php endforeach ?>
</body>
</html>
