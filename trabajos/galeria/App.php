<?php

class App
{
    function __construct()
    {
        session_start();
    }// function __construct
    // iniciar la sesión.

    public function home()
    {
        $directorio = opendir("uploads/");
        $arrayFotos = array();
        while ($archivo = readdir($directorio)){
            if (!is_dir($archivo)){
                array_push($arrayFotos, $archivo);
            }
        }
        require('home.php');// nos lleva al home si iniciamos la sesión.
    }// funcion home

    public function subir()
    {
        $msg = '';
        $subido = true;
        $uploadedfile_size = $_FILES['ficheroSubido']['size'];
        echo $_FILES['ficheroSubido']['name'];
        if ($_FILES['ficheroSubido']['size'] > 200000) {
            $msg = $msg."El archivo es mayor que 200KB, debes reduzcirlo antes de subirlo<br>";
            $subido = false;
        }// if

        if (!($_FILES['ficheroSubido']['type'] =="image/jpeg" || $_FILES['ficheroSubido']['type'] =="image/gif")) {
            $msg = $msg." Tu archivo tiene que ser JPG o GIF.<br>";
            $subido = false;
        }// if

        $file_name = $_FILES['ficheroSubido']['name'];
        $add="/home/alumno/ejerciciosphp/trabajos/galeria/uploads/$file_name";
        if ($subido) {
            if (move_uploaded_file($_FILES['ficheroSubido']['tmp_name'], $add)) {
                echo " Ha sido subido satisfactoriamente";
            }else{
                echo "Error al subir el archivo";
            }// if - else
        }else{
            echo $msg;
        }// if - else
    }// funcion subir

    public function borrar()
    {
        $path ="uploads/";
        $file = $_REQUEST['borrar'];
        if (!unlink($file))
        {
            echo ("Error borrando $file");
        }else{
          echo ("$file borrado");
      }
    }// funcion borrar

    public function detalle()
    {
        var_dump($_POST);
        $pagina =$_POST['detalle'];
        header("Location: $pagina");
    }
}// class App
